import React from 'react';
import ReactDOM from 'react-dom'
import {queryByTestId, render} from "@testing-library/react"
import Issues from '../Issues';
import { Card } from 'react-bootstrap'

test("<Issues /> matches snapshot", () => {
    const component = render(
        <Issues></Issues>
    );
    expect(component.container).toMatchSnapshot();
    
})

describe("Issues", () => {
    it("Component should be titled Issues", () => {
        const wrapper = render(<Issues></Issues>)
        const text = wrapper.getByTestId("issuesDiv")
        expect(text.firstChild?.textContent).toEqual("Issues")

    })
})

