import React from 'react';
import ReactDOM from 'react-dom'
import {queryByTestId, render} from "@testing-library/react"
import Commits from '../Commits';
import { Card } from 'react-bootstrap'

test("<Commits /> matches snapshot", () => {
    const component = render(
        <Commits></Commits>
    );
    expect(component.container).toMatchSnapshot();
    
})

describe("Commits", () => {
    it("Component should be titled Commits", () => {
        const wrapper = render(<Commits></Commits>)
        const text = wrapper.getByTestId("commitsDiv")
        expect(text.firstChild?.textContent).toEqual("Commits")

    })
})

