import axios from 'axios';
import { RSA_X931_PADDING } from 'constants';
import { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import LanguageParse from './LanguageParse';
import BarChart from './BarChart';



export default class Dashboard extends Component {


  state = {
    languages: LanguageParse,
    token: "j_9f1CjhkyHuJaTyLuBb"
  }



  constructor(props: any) {
    super(props)
    axios.get("https://gitlab.stud.idi.ntnu.no/api/v4/projects/11787/languages", {
      headers: {
        'Authorization': `Bearer ${this.state.token}`
      }
    }).then(res => {
      this.setState({ languages: res.data });
      console.log(this.state.languages);
    });
  }

  renderLanguage(languages: LanguageParse) {
    return (
      <>
        <p>CSS: {languages.CSS}%</p>
        <p>HTML: {languages.HTML}%</p>
        <p>TypeScript: {languages.typeScript}%</p>
      </>
    )

  }

  render() {
    return (
      <Container className="border">
        <Row>
          <Col>
            <h3>Languages:</h3>
            <p>{this.renderLanguage(new LanguageParse(this.state.languages))}</p>
            <BarChart languages={new LanguageParse(this.state.languages)} />
          </Col>
          <Col>

          </Col>
        </Row>
      </Container>
    )
  }
}



