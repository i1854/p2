import React from 'react'
import { Bar } from 'react-chartjs-2'
import LanguageParse from './LanguageParse'

type props = {
    languages: LanguageParse
}

const BarChart = ({ languages }: props) => {
    console.log({ languages });
    return (
        <div>
            <Bar
                data={{
                    labels: ['Typescript', 'HTML', 'CSS'],
                    datasets: [{
                        label: "Languages",
                        data: [languages.typeScript, languages.HTML, languages.CSS],
                        backgroundColor: [
                            '#8CD3EF',
                        ],
                    }]
                }}
                width={1000}
                height={400}
                options={{
                    maintainAspectRatio: false
                }}
            />
        </div>
    )
}

export default BarChart
