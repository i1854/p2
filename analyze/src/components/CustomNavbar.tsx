import { useState } from "react"
import Issues from "./Issues";
import Commits from "./Commits";
import { Container, Nav, Navbar } from "react-bootstrap";

const CustomNav = () => {

    const [showIssues, setIVisibility] = useState(false);
    const [showCommits, setCVisibility] = useState(false);

    function IssueVisibility(){
        if(showIssues == false) {
          setIVisibility(true);
          setCVisibility(false)
      }
    }

      function CommitVisibility(){
        if(showCommits == false) {
          setCVisibility(true);
          setIVisibility(false);
        }
      }

    return (
      <Navbar className="NavBar">
        <Container>
          <Nav>
            <Nav.Link href="Issues" id="IssueNav">Issues</Nav.Link>
            <Nav.Link href="Commits" id="CommitsNav">Commits</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    )
}



export default CustomNav