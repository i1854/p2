import axios from 'axios'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import reportWebVitals from '../reportWebVitals'
import Issue from '../Interface/Issue'
import Commit from '../Interface/Commit'
import { BsFillPersonFill } from 'react-icons/bs';
import { Card } from 'react-bootstrap'


export default class Commits extends Component {


    state = {
        commits: [] = localStorage.getItem('commits') ? JSON.parse(localStorage.getItem('commits') || '[]') : [],
        token: "Ztn-Pe8NUW2uN7XZzYjG",
        token2: "uZDhKb7_sg_Zfm7fLpr1",
    }



    //Henter issues fra repoet og setter staten 
    constructor(props: any) {
        super(props);
        axios.get("https://gitlab.stud.idi.ntnu.no/api/v4/projects/11787/repository/commits", {
            //Header for å kunne bruke token
            headers: {
                'Authorization': `Bearer ${this.state.token2}`
            }
        }).then(res => {
            this.setState({ commits: res.data });
            localStorage.setItem('commits', JSON.stringify(this.state.commits))
        });


    }
    //Rendering av hvert enkelt commit
    renderCommit(commit: Commit) {
        return (
            <Card className='m-3 border-0 border-bottom border-light rounded-0'>
                <Card.Body className='border-0'>
                    <Card.Title>{commit.title}</Card.Title>
                    <Card.Subtitle className='text-muted mb-3'>{commit.id}</Card.Subtitle>
                    <Card.Text className='p-1'><BsFillPersonFill />{commit.author_name[0]}</Card.Text>
                </Card.Body>
            </Card >
        )
    }

    //Rendrer komponenten. mapper hvert issue ved hjelp av renderCommit funksjonen
    render() {
        return (
            <div className="commits">
                <h1 className='title mt-2' data-testid="commitsDiv">Commits</h1>
                <ul>
                    {this.state.commits.map(this.renderCommit)}
                </ul>
            </div>
        )
    }
}
