
export default class Language {
    typeScript: number;
    HTML: number;
    CSS: number;

    constructor(response: any){
        this.typeScript = response.TypeScript;
        this.HTML = response.HTML;
        this.CSS = response.CSS;
    }

    
}