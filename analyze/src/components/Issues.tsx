import axios from 'axios'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import reportWebVitals from '../reportWebVitals'
import Issue from '../Interface/Issue'
import { Card } from 'react-bootstrap'
import { BsFillBugFill } from 'react-icons/bs';
import App from "../App"


export default class Issues extends Component {


    state = {
        issues: [] = localStorage.getItem('issues') ? JSON.parse(localStorage.getItem('issues') || '[]') : [],
        // issues: [] = JSON.parse(localStorage.getItem('issues') || '[]'),
        token: "Ztn-Pe8NUW2uN7XZzYjG",
        token2: "uZDhKb7_sg_Zfm7fLpr1",
        visibility: false,
    }



    //Henter issues fra repoet og setter staten 
    constructor(props: any) {
        super(props);
        axios.get("https://gitlab.stud.idi.ntnu.no/api/v4/projects/11787/issues", {
            //Header for å kunne bruke token
            headers: {
                'Authorization': `Bearer ${this.state.token2}`
            }
        }).then(res => {
            this.setState({ issues: res.data });
            localStorage.setItem('issues', JSON.stringify(this.state.issues))
        });


    }

    //Rendering av hvert enkelt issues
    renderIssue(issue: Issue) {
        return (
            <Card className='m-3 border-0 border-bottom border-light rounded-0'>
                <Card.Body className='border-0'>
                    <Card.Title>{issue.title}</Card.Title>
                    <Card.Subtitle className='text-muted mb-3'>{issue.id}</Card.Subtitle>
                    <Card.Text>{issue.description}</Card.Text>
                    <Card.Footer className='m-0 p-1'><BsFillBugFill />Status: {issue.state}</Card.Footer>
                </Card.Body>
            </Card >
        )
    }

    //Rendrer komponenten. mapper hvert issue ved hjelp av renderIssue funksjonen
    render() {
        return (
            <div className="issues" data-testid="issuesDiv">
                <h1 className='title mt-2'>Issues</h1>
                <ul>
                    {this.state.issues.map(this.renderIssue)}
                </ul>
            </div>
        )
    }
}
