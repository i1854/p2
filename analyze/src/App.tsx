import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import Dashboard from './components/Dashboard';
import CustomNav from './components/CustomNavbar';
import Commits from './components/Commits';
import Issues from './components/Issues';




function App() {

  const [showIssues, setshowIssues] = useState(false);
  const [showCommits, setCVisibility] = useState(false);

  function buttonIssues() {
    if (showIssues == false) {
      setshowIssues(true);
    }
    else {
      setshowIssues(false)
    }
  }

  return (
    <>
      <CustomNav />
      <Dashboard />
      <Router>
        <Route path="/Issues">
          <Issues />
        </Route>
        <Route path="/Commits">
          <Commits />
        </Route>
      </Router>
    </>
  );
}

export default App;
