
interface Issue {
    id: string;
    title: string;
    description: string;
    state: string;
}

export default Issue;