interface Commit {
    id: string;
    title: string;
    author_name: string;
}

export default Commit;