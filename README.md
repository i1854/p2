# Project 2 - Visualization

**Hvordan kjøre appen**
<br>
Åpne terminalen og gå inn i analyze mappen ved å skrive `cd analyze` for å koomme inn i applikasjonen. Deretter skriver man `npm i` for å installere de avhengige pakkene. Man kan starte appen lokalt ved å skrive `npm start`. For å kjøre testene til applikasjonen, skriver man `npm test`.

**Funksjonalitet og responsivitet**
<br>
Applikasjonen vår baserer seg på å presentere diverse data fra GitLab-repositoriet vårt. I dette prosjektet har vi valgt å fremvise våre commits og issues, samt en graf som illustrerer mengden av de forskjellige språkene vi har brukt i prosjektet (CSS, HTML og TypeScript).
Hovedårsaken til responsiviteten i appen skyldes bruk av eksternt bibliotek for UI kalt `React Bootstrap`, mer informasjon om dette finnes lengre nede i dokumentasjonen. Nettsiden fungerer fint både på mobile og stasjonære enheter med forskjellige skjermstørrelser og orientasjoner. UI bilbioteket inneholder egen implementasjon av mediaqueries for brytningspunkter i grensesnittet.

**Lagring**
<br>
Vi har brukt Local Storage for å lagre data fra `Issues` og `Commits`. Dataen hentes først fra Local Storage dersom det eksisterer. Deretter vil API-kall til GitLab oppdatere denne lagringen før rendringen av domenet skjer.

**React og typescript** 
<br>
Siden er laget i rammeverket React med typescript som utviklingsspråk. Vi har laget noen interface for å deklarere type på dataen vi hentet fra gitlab. Vi har også enkle komponenter som blir rendret under App.tsx filen. Vi har brukt bootstrap som ui bibliotek for å kunne bruke ferdig komponenter og skape et enkelt og brukervennelig brukergrensesnitt som presenterer dataen på en oversiktlig måte.

**Axios API kall** 
<br>
Axios blir brukt for å kommunisere med apiet. Axios er en av dagens standarder ved API kall, og kommer med sikkerhetsfordeler. Vi har valgt å bruke axios fremfor fetch, av grunnen til at man kan sende asynkrone forespørsler som venter på at resultatet er mottat. Det er kanskje ikke så hensiktsmessig å burke det når alt det ble brukt til er en get request, men vi ville likevel prøve det ut. Vi bruker gitlab sitt api og henter data om commits, issues og languages med axios. For å kunne hente dataen har vi laget en bearer token, og selv om det er dårlig kodeskikk å ha token hardkodet så har vi valgt å gjøre dette for enkelhets skyld i denne oppgaven. Vi valgte å ta med enkel data fra gitlab. Ved valg av hvilken data som skulle presenteres i vår applikasjon, gikk vi for issues, commits og languages fordi det virket mest hensiktsmessige å presentere og siden det enkelt fylte kravene til oppgaven.

**UI og design**
<br>
For å gjøre utformingen av UI litt mer effektiv, har vi tatt i bruk rammeverket/biblioteket Bootstrap som inneholder maler for CSS med fokus på responsivitet og kompatibilitet på tvers av grensesnitt. Fra Boostrap har vi blant annet tatt i bruk komponentet "Card", som gjør presentasjon av grupperinger av data enkelt ved hjelp av elementer som header, footer og body med forhåndsdefinerte stiler.
Utover dette har vi laget en rekke egne React-komponenter som både inneholder vanlige HTML-elementer men også Bootstrap sine innebygde maler, som er brukt i for eksempel **Issues.tsx**.  
Vi har valgt å gå for et mørkt tema i applikasjonen for å skape et miljø som gir et roligere press på øyene under utvikling enn et lysere alternativ. Fargepalletten binder uansett elementene i grensesnittet sammen og gir en helhetelig følelse.    
Et av kravene til oppgaven var inkludering av bilder som skalerer, vi har et grafelement som vi har tatt med istedenfor dette. Det var ingen logisk bakgrunn for inkludering av bilder i vår applikasjon. Ved inkludering av bilde i vår applikasjon kunne vi gjort den responsiv med bootstrap sin stil `.img-fluid.max.width: 100%;` og `height: auto;` slik at bildet skalerer med parent elementet.

**Node og NPM**
<br>
Vi har tatt i bruk både Node og Node Package Manager (Npm) for å installere, oppdatere og fjerne eksterne **Node.js** pakker innad i vårt prosjekt.

**Testing**
<br>
Det er implementert enkle tester for å bli kjent med **react** og **jest** testing. Vi sjekker blant annet at komponenter inneholder forventet informasjon. Det vil si at vi ikke har 100% testdekningsgrad. Men som sagt så er testene kun for læring og for å utfylle oppgavenes krav. Ved større dekningsgrad ville koden kun bli kopiert, noe vi ikke så hensiktsmessig i denne problemstillingen. I tillegg har vi testet ulike skjermstørrelser for å se at siden er responsiv. Dette er gjort manuelt ved bruk av npm sine servere til å koble til via telefonen og ved hjelp av Chrome sitt innebygde responsitivetsverktøy.

**Oppsett av prosjekt**
<br>
Vi brukte `create-react-app project2visualization template typescript` for å sette opp prosjektet.

